<?php

namespace App\Models;

use App\Models\Traits\Named;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Doctor
 *
 * @property-read mixed $display_name
 * @property-read mixed $full_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Hospital[] $hospitals
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WorkingPeriod[] $working_periods
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Doctor newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Doctor newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Doctor query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $specialization
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Doctor whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Doctor whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Doctor whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Doctor whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Doctor whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Doctor whereMiddleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Doctor whereSpecialization($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Doctor whereUpdatedAt($value)
 */
class Doctor extends Model
{
    use Named;

    protected $table = 'doctors';

    protected $guarded = [];

    protected $hidden = [];

    public function hospitals()
    {
        return $this->belongsToMany(Hospital::class, 'doctor_hospital');
    }

    public function working_periods()
    {
        return $this->hasMany(WorkingPeriod::class)->orderBy('hospital_id')->orderBy('date');
    }


}
