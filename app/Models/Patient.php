<?php

namespace App\Models;

use App\Models\Traits\Named;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Patient
 *
 * @method static Builder|Patient newModelQuery()
 * @method static Builder|Patient newQuery()
 * @method static Builder|Patient query()
 * @mixin Eloquent
 * @property int $id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $birthday
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Patient whereBirthday($value)
 * @method static Builder|Patient whereCreatedAt($value)
 * @method static Builder|Patient whereFirstName($value)
 * @method static Builder|Patient whereId($value)
 * @method static Builder|Patient whereLastName($value)
 * @method static Builder|Patient whereMiddleName($value)
 * @method static Builder|Patient whereUpdatedAt($value)
 */
class Patient extends Model
{
    use Named;

    protected $table = 'patients';

    protected $guarded = [];

    protected $hidden = [];
}
