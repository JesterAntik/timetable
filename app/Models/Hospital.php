<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Hospital
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Doctor[] $doctors
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WorkingPeriod[] $working_periods
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hospital newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hospital newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hospital query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $address
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hospital whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hospital whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hospital whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hospital whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hospital whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hospital whereUpdatedAt($value)
 */
class Hospital extends Model
{
    protected $table = 'hospitals';

    protected $guarded = [];

    protected $hidden = [];

    public function doctors()
    {
        return $this->belongsToMany(Doctor::class,'doctor_hospital');
    }

    public function working_periods()
    {
        return $this->hasMany(WorkingPeriod::class);
    }
}
