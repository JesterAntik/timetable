<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Appointment
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $patient_id
 * @property int $working_period_id
 * @property int $doctor_id
 * @property string $start_at
 * @property int $duration
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereDoctorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment wherePatientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereWorkingPeriodId($value)
 */
class Appointment extends Model
{
    //
}
