<?php


namespace App\Models\Traits;


trait Named
{
    public function getFullNameAttribute()
    {

        return implode(' ', array_filter([
            $this->last_name,
            $this->first_name,
            $this->middle_name]));
    }

    public function getDisplayNameAttribute()
    {
        if (!empty($this->first_name))
            $first = mb_strtoupper(mb_substr($this->first_name,0,1)) . '.';

        if (!empty($this->middle_name))
            $middle = mb_strtoupper(mb_substr($this->middle_name,0,1)). '.';
        return implode(' ', array_filter([
            $this->last_name,
            $first,
            $middle
        ]));
    }
}
