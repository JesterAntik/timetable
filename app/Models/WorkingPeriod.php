<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\WorkingPeriod
 *
 * @property-read \App\Models\Doctor $doctor
 * @property-read \App\Models\Hospital $hospital
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkingPeriod newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkingPeriod newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkingPeriod query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $hospital_id
 * @property int $doctor_id
 * @property \Illuminate\Support\Carbon|null $start_at
 * @property \Illuminate\Support\Carbon|null $end_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkingPeriod whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkingPeriod whereDoctorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkingPeriod whereEndAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkingPeriod whereHospitalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkingPeriod whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkingPeriod whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WorkingPeriod whereUpdatedAt($value)
 */
class WorkingPeriod extends Model
{
    protected $table = 'working_periods';

    protected $guarded = [];

    protected $hidden = [];

    protected $dates = [
        'date',
    ];

    public function hospital()
    {
        return $this->belongsTo(Hospital::class);
    }

    public function doctor()
    {
        return $this->belongsTo(Doctor::class);
    }

    public function getStartAttribute($value)
    {
        $time = Carbon::createFromFormat('H:i:s', $value);

        return $time->format('H:i');
    }
    public function getEndAttribute($value)
    {
        $time = Carbon::createFromFormat('H:i:s', $value);

        return $time->format('H:i');
    }

    public function existPeriod()
    {
        return WorkingPeriod::where('doctor_id',$this->doctor_id)
                ->where('date',$this->date)
            ->where(function($query){
                $query->where('start','<',$this->start)
                    ->where('end','>',$this->start);
            })
            ->orWhere(function($query){
                $query->where('start','<',$this->end)
                    ->where('end','>',$this->end);
            })
            ->count() > 0;
    }
}
