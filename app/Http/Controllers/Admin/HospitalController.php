<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\HospitalStoreRequest;
use App\Http\Requests\SyncHospitalDoctorsRequest;
use App\Models\Doctor;
use App\Models\Hospital;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class HospitalController extends AdminController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $hospitals = Hospital::all();
        $title = "Список поликлиник";
        return view('admin.hospitals.index', compact('hospitals', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $title = "Добавить новую поликлинику";
        return view('admin.hospitals.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param HospitalStoreRequest $request
     * @return Response
     */
    public function store(HospitalStoreRequest $request)
    {
        $hospital = Hospital::create($request->all());
        return redirect()->route('admin.hospitals.show', $hospital);
    }

    /**
     * Display the specified resource.
     *
     * @param Hospital $hospital
     * @return Response
     */
    public function show(Hospital $hospital)
    {
        $title = $hospital->name;
        return view('admin.hospitals.show', compact('hospital', 'title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Hospital $hospital
     * @return Response
     */
    public function edit(Hospital $hospital)
    {
        $title = $hospital->name;
        return view('admin.hospitals.edit', compact('hospital', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param HospitalStoreRequest $request
     * @param Hospital $hospital
     * @return Response
     */
    public function update(HospitalStoreRequest $request, Hospital $hospital)
    {
        $hospital->update($request->all());
        return redirect()->route('admin.hospitals.show', $hospital);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Hospital $hospital
     * @return Response
     * @throws Exception
     */
    public function destroy(Hospital $hospital)
    {
        $hospital->delete();
        return redirect()->route('admin.hospitals.index');
    }

    public function doctors(Hospital $hospital)
    {
        $hospital->load('doctors');
        $doctors = Doctor::get()->map(function ($doctor) use ($hospital) {
            $doctor->in_hospital = $hospital->doctors->contains('id', $doctor->id);
            return $doctor;
        });

        return view('admin.hospitals.edit_doctorlist', compact('hospital', 'doctors'));
    }

    public function syncDoctors(SyncHospitalDoctorsRequest $request, Hospital $hospital)
    {
        $hospital->doctors()->sync($request->input('doctors'));
        return redirect()->route('admin.hospitals.show',$hospital);
    }
}
