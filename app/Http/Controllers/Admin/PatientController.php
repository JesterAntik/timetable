<?php

namespace App\Http\Controllers\Admin;

use App\Models\Patient;
use Exception;
use Illuminate\Http\Response;

class PatientController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $patients = Patient::paginate();
        $title = "Список пациентов";
        return view('admin.patients.index', compact('patients','title'));
    }


    /**
     * Display the specified resource.
     *
     * @param Patient $patient
     * @return Response
     */
    public function show(Patient $patient)
    {
        $title = $patient->display_name;
        return view('admin.patients.show', compact('patient', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Patient $patient
     * @return Response
     * @throws Exception
     */
    public function destroy(Patient $patient)
    {
        $patient->delete();
        return redirect()->route('admin.doctors.index');
    }
}
