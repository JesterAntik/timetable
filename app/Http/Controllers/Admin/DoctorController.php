<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DoctorStoreRequest;
use App\Models\Doctor;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DoctorController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $doctors = Doctor::paginate();
        $title = "Список врачей";
        return view('admin.doctors.index', compact('doctors','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $title = "Добавить врача";
        return view('admin.doctors.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DoctorStoreRequest $request
     * @return Response
     */
    public function store(DoctorStoreRequest $request)
    {
        $doctor = Doctor::create($request->all());
        return redirect()->route('admin.doctors.show', $doctor);
    }

    /**
     * Display the specified resource.
     *
     * @param Doctor $doctor
     * @return Response
     */
    public function show(Doctor $doctor)
    {
        $title = $doctor->display_name;
        $periods_by_hospitals = $doctor->working_periods->groupBy('hospital_id');
        return view('admin.doctors.show', compact('doctor', 'title', 'periods_by_hospitals'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Doctor $doctor
     * @return Response
     */
    public function edit(Doctor $doctor)
    {
        $title = $doctor->display_name;
        return view('admin.doctors.edit', compact('doctor', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param DoctorStoreRequest $request
     * @param Doctor $doctor
     * @return Response
     */
    public function update(DoctorStoreRequest $request, Doctor $doctor)
    {
        $doctor->update($request->all());
        return redirect()->route('admin.doctors.show', $doctor);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Doctor $doctor
     * @return Response
     * @throws Exception
     */
    public function destroy(Doctor $doctor)
    {
        $doctor->delete();
        return redirect()->route('admin.doctors.index');
    }
}
