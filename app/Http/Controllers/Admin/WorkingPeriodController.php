<?php

namespace App\Http\Controllers\Admin;

use App\Models\WorkingPeriod;
use Illuminate\Http\Request;

class WorkingPeriodController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WorkingPeriod  $workingPeriod
     * @return \Illuminate\Http\Response
     */
    public function show(WorkingPeriod $workingPeriod)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WorkingPeriod  $workingPeriod
     * @return \Illuminate\Http\Response
     */
    public function edit(WorkingPeriod $workingPeriod)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WorkingPeriod  $workingPeriod
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WorkingPeriod $workingPeriod)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WorkingPeriod  $workingPeriod
     * @return \Illuminate\Http\Response
     */
    public function destroy(WorkingPeriod $workingPeriod)
    {
        //
    }
}
