<?php

namespace App\Http\Controllers;

use App\Models\Doctor;
use App\Models\Hospital;
use App\Models\WorkingPeriod;
use Illuminate\Http\Request;

class WebController extends Controller
{
    public function index()
    {
        $hospitals = Hospital::all();
        return view('index', compact('hospitals'));
    }

    public function hospital(Request $request)
    {
        $this->validate($request,[
            'hospital'=> 'required|integer|exists:hospitals,id'
        ]);
        $hospital = Hospital::find($request->input('hospital'));
        $doctors = $hospital->doctors;
        return view('doctor',compact('doctors', 'hospital'));
    }

    public function doctor(Request $request)
    {
        $this->validate($request,[
            'hospital'=> 'required|integer|exists:hospitals,id',
            'doctor'=> 'required|integer|exists:doctors,id',
        ]);

        $hospital = Hospital::find($request->input('hospital'));
        $doctor = Doctor::find($request->input('doctor'));
        $workingPeriods = WorkingPeriod::where('doctor_id',$doctor->id)
            ->where('hospital_id',$hospital->id)
            ->where('date','>',now())
            ->get();
        return view('time', compact('hospital','doctor','workingPeriods'));
    }
}
