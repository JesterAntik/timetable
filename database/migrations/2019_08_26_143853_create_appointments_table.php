<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('patient_id');
            $table->unsignedBigInteger('working_period_id');
            $table->unsignedBigInteger('doctor_id');
            $table->time('start_at');
            $table->integer('duration')->default(15)->description('Duration in minutes');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('appointments', function(Blueprint $table){
            $table->foreign('patient_id')->on('patients')->references('id')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('working_period_id')->on('working_periods')->references('id')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('doctor_id')->on('doctors')->references('id')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
