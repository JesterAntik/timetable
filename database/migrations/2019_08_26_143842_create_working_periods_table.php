<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkingPeriodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('working_periods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('hospital_id');
            $table->unsignedBigInteger('doctor_id');
            $table->date('date')->index();
            $table->time('start')->nullable()->index();
            $table->time('end')->nullable()->index();
            $table->timestamps();
        });

        Schema::table('working_periods', function(Blueprint $table){
            $table->foreign('hospital_id')->on('hospitals')->references('id')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('doctor_id')->on('doctors')->references('id')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('working_periods');
    }
}
