<?php

use App\Models\Hospital;
use App\Models\Patient;
use App\Models\WorkingPeriod;
use Illuminate\Database\Seeder;

class InitTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $patients = factory(Patient::class, 30);
        $hospitals = factory(App\Models\Hospital::class, 5)->create();

    }
}
