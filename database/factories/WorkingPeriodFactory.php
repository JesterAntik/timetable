<?php

/** @var Factory $factory */

use App\Models\WorkingPeriod;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(WorkingPeriod::class, function (Faker $faker) {
    $date = Carbon::now()->addDays(random_int(3,30));
    $start_hour = random_int(7,17);
    return [
        'date' => $date,
        'start' => $start_hour.":00:00",
        'end' =>  ($start_hour+4).":00:00",
    ];
});
