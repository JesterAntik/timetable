<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Doctor;
use App\Models\Hospital;
use App\Models\WorkingPeriod;
use Faker\Generator as Faker;

$factory->define(Hospital::class, function (Faker $faker) {
    return [
        'name' => "$faker->city hospital",
        'address' => $faker->address,
    ];
});

$factory->afterCreating(Hospital::class, function (Hospital $hospital, $faker) {
   $hospital->doctors()->saveMany(factory(Doctor::class,5)->make());
   foreach ($hospital->doctors as $doctor) {
       factory(WorkingPeriod::class, 20)->make([
           'doctor_id' => $doctor->id,
           'hospital_id' => $hospital->id,
       ])->each(function($workingPeriod){
           if (!$workingPeriod->existPeriod()) $workingPeriod->save();
       });
   }
});
