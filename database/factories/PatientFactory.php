<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Patient;
use Faker\Generator as Faker;

$factory->define(Patient::class, function (Faker $faker) {
    $gender = $faker->randomElement(['male','female']);
    return [
        'first_name'=> $faker->firstName($gender),
        'middle_name' => $faker->middleName($gender),
        'last_name' => $faker->lastName($gender),
        'birthday' => $faker->date(),
    ];
});
