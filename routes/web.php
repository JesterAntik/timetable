<?php

Route::get('/', "WebController@index")->name('index');
Route::POST('/hospital', "WebController@hospital")->name('hospital');
Route::POST('/doctor', "WebController@doctor")->name('doctor');
