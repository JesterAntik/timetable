<?php

use Illuminate\Http\Request;
Route::resource('hospitals', 'HospitalController');
Route::get('hospitals/{hospital}/doctors','HospitalController@doctors')->name('hospitals.doctors.list');
Route::put('hospitals/{hospital}/doctors','HospitalController@syncDoctors')->name('hospitals.doctors.sync');
Route::resource('doctors', 'DoctorController');
Route::resource('patients', 'PatientController');
