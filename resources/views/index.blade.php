@extends('layout.page')

@section('main')
    <div class="justify-content-center">
        <h1 class="pb-2">Запись к врачу</h1>
        <h3>Шаг 1 <small>Выбрать поликлинику</small></h3>
        <form method="post" action="{{route('hospital')}}">
            @csrf
            <table class="table table-sm table-striped">
                <thead>
                <tr>
                    <th></th>
                    <th>Название</th>
                    <th>Адрес</th>
                </tr>
                </thead>
                <tbody>
                @foreach($hospitals as $hospital)
                    <tr>
                        <td><input id="hospital-{{$hospital->id}}" type="radio" name="hospital" value="{{$hospital->id}}">
                        </td>
                        <td><label for="hospital-{{$hospital->id}}">{{$hospital->name}}</label></td>
                        <td>{{$hospital->address}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="d-flex justify-content-between">
                <a href="#" role="button" aria-disabled="true" class="btn btn-info disabled" >Вернуться</a>
                <button type="submit" class="btn btn-success">Далее</button>
            </div>
        </form>

    </div>
@endsection
