@extends('layout.page')

@section('main')
    <div class="justify-content-center">
        <h1 class="pb-2">Запись к врачу</h1>
        <h3>Шаг 2 <small>Выбрать врача</small></h3>
        <form method="post" action="{{route('doctor')}}">
            <input type="hidden" name="hospital" value="{{$hospital->id}}">
            @csrf
            <table class="table table-sm table-striped">
                <thead>
                <tr>
                    <th></th>
                    <th>ФИО</th>
                    <th>Специализация</th>
                </tr>
                </thead>
                <tbody>
                @foreach($doctors as $doctor)
                    <tr>
                        <td><input id="doctor-{{$doctor->id}}" type="radio" name="doctor" value="{{$doctor->id}}">
                        </td>
                        <td><label for="doctor-{{$doctor->id}}">{{$doctor->full_name}}</label></td>
                        <td>{{$doctor->specialization}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="d-flex justify-content-between">
                <a href="{{route('index')}}" role="button" aria-disabled="true" class="btn btn-info" >Вернуться</a>
                <button type="submit" class="btn btn-success">Далее</button>
            </div>
        </form>

    </div>
@endsection
