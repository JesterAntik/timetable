@extends('admin.layout.page')
@section('main')
    <div class="card-columns">
        <div class="card p-2">
            <ul class="list-unstyled">
                <li><b>Название</b>: {{$hospital->name}}</li>
                <li><b>Адрес</b>: {{$hospital->address}}</li>
            </ul>
            <a href="{{route('admin.hospitals.edit',$hospital)}}">Редактировать</a>
        </div>
        <div class="card p-3">
            <h3>Приписанные врачи</h3>
            <a href="{{route('admin.hospitals.doctors.list',$hospital)}}">Редактировать список</a>
            <ul>
                @foreach($hospital->doctors as $doctor)
                <li><a href="{{route('admin.doctors.show',$doctor)}}">{{$doctor->full_name}}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection
