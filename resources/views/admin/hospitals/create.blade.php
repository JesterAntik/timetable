@extends('admin.layout.page')

@section('main')
    <div class="container justify-content-center">
        <div class="card p-2">
            <form action="{{route('admin.hospitals.store')}}" method="post">
                @csrf()
                @method('POST')
                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" class="@error('name') is-invalid @enderror form-control" id="name" placeholder="Больница им. Святого Гиппократа"
                           name="name"
                           value="{{old('name')}}">
                    @error('name')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="address">Адрес</label>
                    <input type="text" class="form-control" id="address" placeholder="Невский пр-кт, д 1"
                           name="address"
                           value="{{old('address')}}">
                    @error('address')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-success">Сохранить</button>
            </form>
        </div>
    </div>
@endsection
