@extends('admin.layout.page')

@section('main')
    <div class="container justify-content-center">
        <div class="card p-2">
            <form action="{{route('admin.hospitals.update', $hospital)}}" method="post">
                @csrf()
                @method('PUT')
                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" class="@error('name') is-invalid @enderror form-control" id="name" placeholder="Больница им. Святого Гиппократа"
                           name="name"
                           required value="{{$hospital->name}}">
                    @error('name')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="address">Адрес</label>
                    <input type="text" class="@error('address') is-invalid @enderror form-control" id="address" placeholder="Невский пр-кт, д 1" required
                           name="address"
                           value="{{$hospital->address}}">
                    @error('address')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-success">Сохранить</button>
            </form>
            <form action="{{route('admin.hospitals.destroy',$hospital)}}" method="post">
                @csrf()
                @method('DELETE')
                <button type="submit"  class="btn btn-outline-danger mt-3">Удалить</button></form>
        </div>
    </div>
@endsection
