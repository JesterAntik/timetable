@extends('admin.layout.page')

@section('main')
    <div class="container justify-content-center">
        <a href="{{route('admin.hospitals.create')}}" class="d-block my-2">Добавить поликлинику</a>
        <table class="table table-sm table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Название</th>
                <th>Адрес</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($hospitals as $patient)
                <tr >
                    <td>{{$patient->id}}</td>
                    <td><a href="{{route('admin.hospitals.show',$patient)}}">{{$patient->name}}</a></td>
                    <td>{{$patient->address}}</td>
                    <td><form action="{{route('admin.hospitals.destroy',$patient)}}" method="post">
                            @csrf()
                            @method('DELETE')
                            <button type="submit"  class="btn btn-sm btn-outline-danger">Удалить</button></form></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
