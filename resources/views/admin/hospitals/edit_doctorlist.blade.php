@extends('admin.layout.page')

@section('main')
    <div class="container justify-content-center">
        <div class="card p-2">
            <h2>Список врачей в {{$hospital->name}}</h2>
            <form action="{{route('admin.hospitals.doctors.sync', $hospital)}}" method="post">
                @csrf()
                @method('PUT')
                @foreach($doctors as $doctor)
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="doctor-{{$doctor->id}}"
                               name="doctors[]" value="{{$doctor->id}}"
                               @if ($doctor->in_hospital) checked @endif>
                        <label class="custom-control-label" for="doctor-{{$doctor->id}}">{{$doctor->full_name}}</label>
                    </div>
                @endforeach
                <button type="submit" class="btn btn-success mt-2">Сохранить</button>
            </form>
        </div>
    </div>
@endsection
