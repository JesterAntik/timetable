@extends('admin.layout.page')
@section('main')
    <div class="card-deck">
        <div class="card p-2">
            <ul class="list-unstyled">
                <li><b>Фамилия</b>: {{$doctor->last_name}}</li>
                <li><b>Имя</b>: {{$doctor->first_name}}</li>
                <li><b>Отчество</b>: {{$doctor->middle_name}}</li>
                <li><b>Специализация</b>: {{$doctor->specialization}}</li>
            </ul>
            <a href="{{route('admin.doctors.edit',$doctor)}}">Редактировать</a>
        </div>
        <div class="card p-3">
            <h3>Поликлиники</h3>
            <ul>
                @foreach($doctor->hospitals as $hospital)
                    <li><a href="{{route('admin.hospitals.show',$hospital)}}">
                            {{$hospital->name}}</a></li>
                @endforeach
            </ul>
        </div>
        <div class="card p-3">
            <h3>Расписание</h3>
            @foreach($periods_by_hospitals as $working_periods)
            <h4> <a href="{{route('admin.hospitals.show',$working_periods->first()->hospital)}}">
                    {{$working_periods->first()->hospital->name}}</a></h4>
            <table class="table table-sm table-striped">
                <thead>
                <tr>
                    <th>Дата</th>
                    <th>Часы работы</th>
                </tr>
                </thead>
                <tbody>
                @foreach($working_periods as $workingPeriod)
                    <tr>
                        <td>
                           {{$workingPeriod->date->format('j M')}}
                        </td>
                        <td>
                            {{$workingPeriod->start}} - {{$workingPeriod->end}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @endforeach

        </div>
    </div>
@endsection
