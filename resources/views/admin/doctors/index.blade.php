@extends('admin.layout.page')

@section('main')
    <div class="container justify-content-center">
        <a href="{{route('admin.doctors.create')}}" class="d-block my-2">Добавить врача</a>
        <table class="table table-sm table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Имя</th>
                <th>Специализация</th>
                <th>Расписание</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($doctors as $doctor)
                <tr >
                    <td>{{$doctor->id}}</td>
                    <td><a href="{{route('admin.doctors.show',$doctor)}}">{{$doctor->full_name}}</a></td>
                    <td>{{$doctor->specialization}}</td>
                    <td></td>
                    <td><form action="{{route('admin.doctors.destroy',$doctor)}}" method="post">
                            @csrf()
                            @method('DELETE')
                            <button type="submit"  class="btn btn-sm btn-outline-danger">Удалить</button></form></td>
                </tr>
            @endforeach
            </tbody>
            <caption>
                {{$doctors->links()}}
            </caption>
        </table>

    </div>
@endsection
