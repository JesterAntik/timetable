@extends('admin.layout.page')

@section('main')
    <div class="container justify-content-center">
        <div class="card p-2">
            <form action="{{route('admin.doctors.update', $doctor)}}" method="post">
                @csrf()
                @method('PUT')
                <div class="form-group">
                    <label for="last_name">Фамилия</label>
                    <input type="text" class="@error('last_name') is-invalid @enderror form-control"
                           id="last_name"
                           name="last_name"
                           value="{{ $doctor->last_name}}">
                    @error('last_name')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="first_name">Имя</label>
                    <input type="text" class="@error('first_name') is-invalid @enderror form-control"
                           id="first_name"
                           name="first_name"
                           value="{{$doctor->first_name}}">
                    @error('first_name')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="middle_name">Отчество</label>
                    <input type="text" class="@error('middle_name') is-invalid @enderror form-control"
                           id="middle_name"
                           name="middle_name"
                           value="{{$doctor->middle_name}}">
                    @error('middle_name')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="specialization">Специализация</label>
                    <input type="text" class="@error('specialization') is-invalid @enderror form-control"
                           id="specialization"
                           name="specialization"
                           value="{{$doctor->specialization}}">
                    @error('specialization')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-success">Сохранить</button>
            </form>
        </div>
    </div>
@endsection
