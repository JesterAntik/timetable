@extends('admin.layout.page')
@section('main')
    <div class="card-columns">
        <div class="card p-2">
            <ul class="list-unstyled">
                <li><b>Фамилия</b>: {{$patient->last_name}}</li>
                <li><b>Имя</b>: {{$patient->first_name}}</li>
                <li><b>Отчество</b>: {{$patient->middle_name}}</li>
                <li><b>Дата рождения</b>: {{$patient->birthday}}</li>
            </ul>
            <a href="{{route('admin.patients.edit',$patient)}}">Редактировать</a>
        </div>
        <div class="card p-3">
        </div>
    </div>
@endsection
