@extends('admin.layout.page')

@section('main')
    <div class="container justify-content-center">
        <table class="table table-sm table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Имя</th>
                <th>Дата рождения</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($patients as $patient)
                <tr >
                    <td>{{$patient->id}}</td>
                    <td><a href="{{route('admin.patients.show',$patient)}}">{{$patient->full_name}}</a></td>
                    <td>{{$patient->birthday}}</td>
                    <td><form action="{{route('admin.patients.destroy',$patient)}}" method="post">
                            @csrf()
                            @method('DELETE')
                            <button type="submit"  class="btn btn-sm btn-outline-danger">Удалить</button></form></td>
                </tr>
            @endforeach
            </tbody>
            <caption>
                {{$patients->links()}}
            </caption>
        </table>

    </div>
@endsection
